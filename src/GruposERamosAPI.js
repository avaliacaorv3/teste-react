const api = process.env.REACT_APP_API_URL;

let token = localStorage.token
if (!token)
  token = localStorage.token = Math.random().toString(36).substr(-8)

const headers = {
  'Accept': 'application/json',
  'Authorization': token
}

export const getGrupos = () =>
  fetch(`${api}/grupos`, { headers })
    .then(res => res.json())
    .then(data => data);

export const getRamos = () =>
  fetch(`${api}/ramos`, { headers })
    .then(res => res.json())
    .then(data => data);

export const getRamosDoGrupo = (numeroDoGurpo) =>
  fetch(`${api}/grupos/${numeroDoGurpo}/ramos`, { headers })
    .then(res => res.json())
    .then(data => data);
