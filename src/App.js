import React, { Component } from 'react';
import './App.css';

import * as GruposERamosAPI from './GruposERamosAPI';

let grupoSelecionado;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      grupos: null,
      ramos: null
    };
  };

  componentDidMount() {
    GruposERamosAPI.getGrupos().then((grupos) => {
      this.setState(() => ({ grupos }));
    });
  };

  onSelectGrupo = (numeroDoGrupo) => {
    GruposERamosAPI.getRamosDoGrupo(numeroDoGrupo).then((ramos) => {
      this.setState(() => ({ ramos }));

      grupoSelecionado = numeroDoGrupo;
    })
  };

  render() {
    const { grupos, ramos } = this.state;

    return (
      <div className="App">
        <header>
          <span>Lista de Grupos e Ramos SUSEP - Circular Susep Nº 455, de 6 de desembro de 2012</span>
        </header>
        <h1>Grupos</h1>
        <select onChange={(e) => this.onSelectGrupo(e.target.value)}>
          <option value="0">Selcione o Grupo</option>
          {grupos != null && grupos.map((grupo) => (
            <option value={grupo.numero}>{grupo.numero} - {grupo.nome}</option>
          ))}
        </select>
        <hr />
        {ramos != null && Array.isArray(ramos) && ramos.length > 0 && (
          <div>
          <h2>Ramos do grupo {grupoSelecionado}</h2>
          <table className="tblRamos">
            <tr>
               <th>Ramo</th>
              <th>Nome</th>
            </tr>
            {ramos.map((ramo) => (
              <tr>
                <td>{ramo.numero}</td>
                <td>{ramo.nome}</td>
              </tr>
            ))}
          </table>
          </div>
        )}
      </div>
    );
  }
}

export default App;
